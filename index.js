import { AppRegistry} from 'react-native';
import React, {Component} from 'react';
import App from './App';
import { Tester, TestHookStore } from 'cavy';
import TestAuto from './cavyTest/auto-mode-test';
import TestManual from './cavyTest/manual-mode-test';

const testHookStore = new TestHookStore();

export default class AppWrapper extends Component {
  render() {
    return (
      <Tester specs={[TestManual]} store={testHookStore} waitTime={4000}>
        <App />
      </Tester>
    );
  }
}

// AppRegistry.registerComponent('Specter', () => AppWrapper);
AppRegistry.registerComponent('Specter', () => App);
