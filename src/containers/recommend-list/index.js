import React, { Component } from 'react';
import {
  View,
  FlatList,
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../theme";

// Custom component
import AppText from "../../components/app-text";
import Button from "../../components/custom-button";
import RecommendSpec from "../../components/recommend-spec";
import TitileIndicator from "../../components/title-indicator";

import { Actions } from "react-native-router-flux";

class RecommendList extends Component {

  _navigateToSpecDetail(item) {
    Actions.push('Spec-detail', {
      spec: item,
      mode: this.props.mode,
    });
  }

  _totalPrice(item) {
    let price = 0;
    for (let compKey in item) {
      price += item[compKey].Price;
    }
    return price;
  }

  render() {
    const marginX = this.props.marginX == null ? null : { marginHorizontal: '5%' };
    const recommends = this.props.recommendSpecs;
    const cardStyle = this.props.cardStyle == null ? null : global.card;
    let key = 0;
    return (
      <View style={[local.container, marginX]}>
        <TitileIndicator noBorder={this.props.noBorder} invert={this.props.invert} value="Recommend specs" />
        <FlatList
          contentContainerStyle={[cardStyle, local.listContainer]}
          data={recommends}
          renderItem={({ item }) => {
            const totalPrice = this._totalPrice(item);
            return (
              <RecommendSpec
                onPress={() => this._navigateToSpecDetail(item)}
                id={1 + key++}
                score={item.score}
                price={totalPrice} />
            );
          }}
          keyExtractor={(item) => recommends.indexOf(item)}
        />
      </View>
    );
  }
}

export default RecommendList;