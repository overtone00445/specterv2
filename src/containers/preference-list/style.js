import { StyleSheet } from "react-native";
import { colors } from "../../theme";

export const local = StyleSheet.create({
  container: {
    flex: 1,
  },
  customCard: {
    paddingTop: 12,
    paddingBottom: 0,
    paddingRight: 0,
  },
  modalContainer: {
    borderWidth: 0,
    marginVertical: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    elevation: 4,
    backgroundColor: colors.whiteC,
    padding: 10,
    borderRadius: 4,
  },
  modalTitle: {
    paddingLeft: 5,
    marginBottom: 5
  },
  titleContainer: {
    flexDirection: 'row',
  },
  titleIndicatorContainer: {
    flex: 1
  },
  saveButton: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.greenB,
    borderRadius: 20,
    paddingVertical: 2,
    paddingHorizontal: 10,
  },
  saveIcon: {
    tintColor: colors.whiteB,
    width: 16,
    height: 16,
    marginRight: 4,
  },
  textInput: {
    color: colors.whiteF,
    padding: 5,
  },
  confirmContainer: {
    marginTop: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  confirmBtn: {
    flex:1,
    backgroundColor: colors.greenB,
    borderRadius: 20,
    paddingVertical: 2,
    paddingHorizontal: 15,
  },
  scrollView: {
    flexGrow: 1,
    justifyContent: 'center',
  },
});
