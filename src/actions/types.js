export const ADD_TO_MY_SPEC = 'save_spec';
export const REMOVE_FROM_MY_SPEC = 'remove_from_my_spec';
export const CLEAR_MY_SPEC = 'clear_my_spec';
export const GET_COMPONENT = 'get_component';
export const GET_RECOMMEND_FOR_AUTO = 'get_auto';
export const GET_PARTIAL_FOR_AUTO = 'get_partial_auto';
export const GET_RECOMMEND_FOR_MANUAL = 'get_manual';

export const SET_RECOMMEND_FOR_AUTO = 'set_auto';
export const SET_RECOMMEND_FOR_MANUAL = 'set_manual';

export const SERVICE_PENDING = 'service_pending';
export const SERVICE_SUCCESS = 'service_success';
export const SERVICE_ERROR = 'service_error';

export const SET_PRODUCT_LIST = 'set_product_list';
export const SORT_PRODUCT_NAME_ASC = 'sort_product_name_asc';
export const SORT_PRODUCT_NAME_DES = 'sort_product_name_des';
export const SORT_PRODUCT_PRICE_ASC = 'sort_product_price_asc';
export const SORT_PRODUCT_PRICE_DES = 'sort_product_price_des';

export const ADD_TO_FAVORITE = 'add_to_favorite';
export const REMOVE_FROM_FAVORITE = 'remove_from_favorite';
export const SET_FAVORITE_SPECS = 'set_favorite_specs';

export const LOAD_FAVORITE_FROM_STORAGE = 'load_favorite';
export const LOAD_PRODUCT_FROM_STORAGE = 'load_product';

export const CHANGE_COMPONENT = 'change_component';
export const REMOVE_COMP_FROM_FAVORITE = 'remove_comp_from_favorite';

export const RESPONSE_GET_AUTO = 'response_get_auto';
