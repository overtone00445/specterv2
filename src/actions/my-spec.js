import {
  ADD_TO_MY_SPEC,
  REMOVE_FROM_MY_SPEC,
  CLEAR_MY_SPEC
} from "./types";

export const addToMySpec = (comp) => {
  return {
    type: ADD_TO_MY_SPEC,
    payload: { newComp: comp }
  }
}

export const removeFromMySpec = (idx) => {
  return {
    type: REMOVE_FROM_MY_SPEC,
    payload: { removeIdx: idx }
  }
}

export const clearSpec = () => {
  return {
    type: CLEAR_MY_SPEC
  }
}