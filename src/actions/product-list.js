import {
  SORT_PRODUCT_NAME_ASC,
  SORT_PRODUCT_NAME_DES,
  SORT_PRODUCT_PRICE_ASC,
  SORT_PRODUCT_PRICE_DES,
  SET_PRODUCT_LIST
} from "./types";

export const setProductList = (products, componentType) => {
  return {
    type: SET_PRODUCT_LIST,
    payload: { products, componentType }
  }
}

export const sortProductNameASC = (products, componentType) => {
  return {
    type: SORT_PRODUCT_NAME_ASC,
    payload: { products, componentType }
  }
}

export const sortProductNameDES = (products, componentType) => {
  return {
    type: SORT_PRODUCT_NAME_DES,
    payload: { products, componentType }
  }
}

export const sortProductPriceASC = (products, componentType) => {
  return {
    type: SORT_PRODUCT_PRICE_ASC,
    payload: { products, componentType }
  }
}

export const sortProductPriceDES = (products, componentType) => {
  return {
    type: SORT_PRODUCT_PRICE_DES,
    payload: { products, componentType}
  }
}