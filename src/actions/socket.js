import { GET_RECOMMEND_FOR_AUTO } from "./types";

export const getAutoRecommendBySocket = (message) => {
  return {
    type: 'server/' + GET_RECOMMEND_FOR_AUTO,
    payload: { ...message }
  }
}
