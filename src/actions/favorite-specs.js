import {
  ADD_TO_FAVORITE,
  REMOVE_FROM_FAVORITE,
  LOAD_FAVORITE_FROM_STORAGE,
  SET_FAVORITE_SPECS,
  REMOVE_COMP_FROM_FAVORITE,
  CHANGE_COMPONENT
} from "./types";

export const addToFavorite = (spec, specName, mode) => {
  spec.name = specName;
  spec.mode = mode;

  return {
    type: ADD_TO_FAVORITE,
    payload: {
      newSpec: spec
    }
  }
}

export const loadFavoriteSpecs = () => {
  return {
    type: LOAD_FAVORITE_FROM_STORAGE
  }
}

export const setFavoriteSpecs = (specs) => {
  return {
    type: SET_FAVORITE_SPECS,
    payload: { specs }
  }
}

export const removeFromFavorite = (idx) => {
  return {
    type: REMOVE_FROM_FAVORITE,
    payload: { removeIdx: idx }
  }
}

export const removeCompFromFavorite = (idx) => {
  return {
    type: REMOVE_COMP_FROM_FAVORITE,
    payload: { removeIdx: idx }
  }
}

export const changeComponent = (specIdx, compIdx, newComp) => {
  return {
    type: CHANGE_COMPONENT,
    payload: {
      specIdx,
      compIdx,
      newComp
    }
  }
}