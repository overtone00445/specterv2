import { Spec } from './Spec';

class Spec1 extends Spec {
  constructor() {
    this.components = []
    this.price = this.computePrice()
    this.score = this.computePerformanceScore()
  }

  addComponent(self, node) {
    this.components.append(node)
    this.price = this.computePrice()
    this.score = this.computePerformanceScore()
  }
  computePrice() {
    price = 0
    for (let node of this.components)
      price += node.getPrice()
    return price
  }

  computePerformanceScore() {
    score = 0
    for (let node of this.components)
      score += node.performanceScore()
    return score
  }

  getPrice() {
    return this.price
  }

  performanceScore() {
    return this.score
  }

  getComponents() {
    return this.components
  }

}
