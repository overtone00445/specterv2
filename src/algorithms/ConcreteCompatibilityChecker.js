import { CompatibilityChecker } from './CompatibilityChecker';

class ConcreteCompatibilityChecker extends CompatibilityChecker {

  checkCompatibility(spec) {
    return (this.checkMainBoardCompatibility(spec) && this.checkPowerConsumption(spec))
  }

  getChipset(generation) {
    if (generation == '1')
      return ['H55', 'H67', 'Q57', 'P55']
    else if (generation == '2')
      return ['H61', 'H67', 'Q65', 'Q67', 'B65', 'Z68', 'P65']
    else if (generation == '3')
      return ['H77', 'Q75', 'Q77', 'B75', 'Z75', 'Z77']
    else if (generation == '4')
      return ['H81', 'H87', 'Q85', 'Q87', 'B85', 'Z87']
    else if (generation == '6')
      return ['H110', 'H170', 'Q150', 'Q170', 'B150', 'Z170']
    else if (generation == '7')
      return ['H270', 'Q250', 'Q270', 'B250', 'Z270']
    else if (generation == '8')
      return ['Z370']
    else if (generation == 'x1')
      return ['A320', 'A370', 'X470']
    else if (generation == 'x2')
      return ['X399']
    else
      return ['H270', 'Q250', 'Q270', 'B250', 'Z270']
  }

  getGeneration(title) {
    title = title.split(' ');
    if (title[0] == "INTEL") {
      gen = title[3]
      if (gen[0] == "G")
        generation = gen[1]
      else
        generation = gen[3]
    }
    else if (title[0] == "test_CPU")
      generation = 3
    else if (title[1] == "AM4")
      generation = "x1"
    else if (title[1] == "TR4")
      generation = "x2"
    return generation
  }

  checkMainBoardCompatibility(spec) {
    compatible = true
    mainboardPresent = false
    CPUPresent = false
    RAMPresent = false
    for (let component of spec.getComponents()) {
      if (component.getType().includes("Mainboard")) {
        mainboardPresent = true
        socket = component.getAttributes()["Socket"]
        CPU_series = component.getAttributes()["CPU_Series"]
        chipset = component.getAttributes()["Chipset"]
        ram_type = component.getAttributes()["RAM_Type"]
        break
      }
    }
    for (let component of spec.getComponents()) {
      if (component.getType().includes("CPU")) {
        CPUPresent = true
        CPU_socket = component.getAttributes()["Socket"]
        generation = this.getGeneration(component.getAttributes()["Title"])
        break
      }
    }
    for (let component of spec.getComponents()) {
      if (component.getType().includes("RAM")) {
        RAMPresent = true
        RAM_type = component.getAttributes()["RAM_Type"]
        break
      }
    }
    if (!mainboardPresent && (CPUPresent || RAMPresent))
      return true
    if (CPUPresent && !this.getChipset(generation).includes(chipset))
      compatible = false
    if (CPUPresent && socket != CPU_socket)
      compatible = false
    if (RAMPresent && RAM_type != ram_type)
      compatible = false
    return compatible
  }

  checkPowerConsumption(spec) {
    inputPower = 0
    outputPower = 0
    for (let component of spec.getComponents()) {
      if (component.getType().includes("PowerSupply"))
        inputPower = component.getAttributes()["Max_Power"]
    }
    for (let component of spec.getComponents()) {
      if (component.getType().includes("CPU"))
        outputPower += component.getAttributes()["Power_Peak"]
    }
    for (let component of spec.getComponents()) {
      if (component.getType().includes("GPU"))
        outputPower += component.getAttributes()["Power_Supply"]
    }
    if (inputPower != 0 && outputPower > inputPower - 50)
      return false
    else
      return true
  }

}