import {
  ADD_TO_FAVORITE,
  REMOVE_FROM_FAVORITE,
  SET_FAVORITE_SPECS,
  REMOVE_COMP_FROM_FAVORITE,
  CHANGE_COMPONENT,
} from "../actions/types";

const favoriteSpecs = [];

function decorateSpec(newSpec) {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; //January is 0!
  let yyyy = today.getFullYear();
  today = mm + '/' + dd + '/' + yyyy;
  newSpec.date = today;

  let price = 0;
  for (let comp of newSpec.components) {
    price += comp.Price;
  }
  newSpec.price = price;

  return newSpec;
}

export default reducerFavoriteSpecs = (state = favoriteSpecs, action) => {
  switch (action.type) {

    case ADD_TO_FAVORITE: {
      const newSpec = decorateSpec(action.payload.newSpec);
      const nextState = [...state];
      nextState.unshift(newSpec);
      return nextState;
    } break;

    case REMOVE_FROM_FAVORITE: {
      const nextState = [...state];
      nextState.splice(action.payload.removeIdx, 1);
      return nextState;
    } break;

    case SET_FAVORITE_SPECS: {
      const nextState = action.payload.specs;
      if (nextState == null) { return state; }
      return nextState;
    } break;

    case CHANGE_COMPONENT: {
      const nextState = [...state];
      let currentSpec = nextState[action.payload.specIdx];
      currentSpec.components[action.payload.compIdx] = action.payload.newComp;
      const newSpec = decorateSpec(currentSpec);
      return nextState;
    } break;

    case REMOVE_COMP_FROM_FAVORITE: {
      const nextState = [...state];


    } break;

    default:
      return state;
  }
}