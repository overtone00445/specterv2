import {
  SORT_PRODUCT_NAME_ASC,
  SORT_PRODUCT_NAME_DES,
  SORT_PRODUCT_PRICE_ASC,
  SORT_PRODUCT_PRICE_DES,
  SET_PRODUCT_LIST,
  LOAD_PRODUCT_FROM_STORAGE,
  GET_COMPONENT
} from "../actions/types";

const cachedProducts = {
  cpu: {
    isLoadFirstTime: true,
    products: [],
  },
  mainboard: {
    isLoadFirstTime: true,
    products: [],
  },
  gpu: {
    isLoadFirstTime: true,
    products: [],
  },
  harddisk: {
    isLoadFirstTime: true,
    products: [],
  },
  ssd: {
    isLoadFirstTime: true,
    products: [],
  },
  ram: {
    isLoadFirstTime: true,
    products: [],
  },
  powersupply: {
    isLoadFirstTime: true,
    products: [],
  },
  monitor: {
    isLoadFirstTime: true,
    products: [],
  },
}

export default reducerProductList = (state = cachedProducts, action) => {
  // Don't modify state directly
  switch (action.type) {
    
    case GET_COMPONENT: {
      const nextState = Object.assign({}, state);
      const componentType = action.payload.componentType;
      return nextState;
    } break;

    case SET_PRODUCT_LIST: {
      const nextState = Object.assign({}, state);
      const componentType = action.payload.componentType;
      nextState[componentType].products = action.payload.products;
      nextState[componentType].isLoadFirstTime = false;
      return nextState;
    } break;

    case SORT_PRODUCT_NAME_ASC: {
      const nextState = Object.assign({}, state);
      const componentType = action.payload.componentType;
      products = nextState[componentType].products;
      products.sort(
        function (a, b) {
          if (a.Title > b.Title) return 1;
          else if (a.Title < b.Title) return -1;
          else return 0;
        }
      );
      return nextState;
    } break;

    case SORT_PRODUCT_NAME_DES: {
      const nextState = Object.assign({}, state);
      const componentType = action.payload.componentType;
      products = nextState[componentType].products;
      products.sort(
        function (a, b) {
          if (a.Title < b.Title) return 1;
          else if (a.Title > b.Title) return -1;
          else return 0;
        }
      );
      return nextState;
    } break;

    case SORT_PRODUCT_PRICE_ASC: {
      const nextState = Object.assign({}, state);
      const componentType = action.payload.componentType;
      products = nextState[componentType].products;
      products.sort(
        function (a, b) {
          if (a.Price > b.Price) return 1;
          else if (a.Price < b.Price) return -1;
          else return 0;
        }
      );
      return nextState;
    } break;

    case SORT_PRODUCT_PRICE_DES: {
      const nextState = Object.assign({}, state);
      const componentType = action.payload.componentType;
      products = nextState[componentType].products;
      products.sort(
        function (a, b) {
          if (a.Price < b.Price) return 1;
          else if (a.Price > b.Price) return -1;
          else return 0;
        }
      );
      return nextState;
    } break;

    default:
      return state;
  }
}