import reducerMySpec from "./my-spec";
import reducerService from './service';
import reducerProductList from "./product-list";
import reducerRecommendSpec from "./recommends";
import reducerFavoriteSpecs from "./favorite-specs";

import { combineReducers } from "redux";

export const allReducers = combineReducers({
  mySpec: reducerMySpec,
  service: reducerService,
  products: reducerProductList,
  recommends: reducerRecommendSpec,
  favoriteSpecs: reducerFavoriteSpecs,
});
