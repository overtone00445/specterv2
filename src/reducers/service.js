import {
  SERVICE_PENDING,
  SERVICE_SUCCESS,
  SERVICE_ERROR
} from '../actions/types';

const initialState = {
  isLoading: false,
  error: undefined,
  data: {}
}

import {
  Alert
} from 'react-native';

export default serviceReducer = (state = initialState, action) => {
  switch (action.type) {
    case SERVICE_PENDING:
      return Object.assign({}, state, {
        isLoading: true
      }); break;
    case SERVICE_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        data: action.payload.response
      }); break;
    case SERVICE_ERROR: {
      Alert.alert('Alert', 'Components mismatched or budget was too low !');
      return Object.assign({}, state, {
        isLoading: false,
        error: action.payload.error
      });
    } break;
    default:
      return state;
  }
}