import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  LayoutAnimation,
  ToastAndroid
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../../theme";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

// Container component
import BudgetForm from "../../../containers/budget-form";
import RecommendList from "../../../containers/recommend-list";

// Custom component
import AppText from "../../../components/app-text";
import Button from "../../../components/custom-button";
import SelectComponentList from "../../../components/select-comp-list";
import PreferenceList from "../../../containers/preference-list";

import { Actions } from "react-native-router-flux";

import { getManualRecommend } from "../../../actions/request-api";

const mapStateToProps = (state) => {
  return {
    mySpec: state.mySpec,
    service: state.service,
    recommends: state.recommends,
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getManualRecommend: getManualRecommend
  }, dispatch);
}

class Manual extends Component {

  _generateSpec(budget, pcType) {
    const isInvalid = (budget == ''
      || isNaN(budget)
      || Number.parseInt(budget) <= 0);

    if (isInvalid == true) {
      alert('Invalid input');
      return;
    } else {
      this.props.getManualRecommend(parseInt(budget), this.props.mySpec.components, pcType);
    }
  }

  componentWillReceiveProps(props) {
    Actions.refresh();
  }

  componentDidUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  _navigateToProductList(title, requestName) {
    Actions.push('Product-list', {
      title,
      requestName,
    })
  }

  _renderPreferenceList() {
    if (this.props.mySpec.components.length > 0) {
      return <PreferenceList mode="Manual" />
    }
  }

  _renderRecommendList() {
    let mode = "Auto";
    const recommendSpecs = this.props.recommends.manual;
    if (this.props.mySpec.components.length > 0) {
      mode = "Manual";
    }

    if (Object.keys(recommendSpecs).length > 0) {
      return <RecommendList mode={mode} recommendSpecs={recommendSpecs} cardStyle />
    }
  }

  render() {
    return (
      <View style={global.pageContainer}>
        <ScrollView contentContainerStyle={global.pageScrollView}>
          {this._renderPreferenceList()}


          <View style={local.budgetFormContainer}>
            <BudgetForm isLoading={this.props.service.isLoading} onGenerate={(value, pcType) => { this._generateSpec(value, pcType) }} />
          </View>

          {this._renderRecommendList()}
          <SelectComponentList onSelect={(title, requestName) => this._navigateToProductList(title, requestName)} />

        </ScrollView>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Manual);