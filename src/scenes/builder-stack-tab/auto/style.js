import { StyleSheet } from "react-native";

import { colors } from "../../../theme";

export const local = StyleSheet.create({
  pageContainer: {
    backgroundColor: colors.greenC,
  },
  budgetFormContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  spinner: {
    alignSelf: 'center'
  },
  formTitle: {
    alignItems: 'center',
    marginVertical: 10,
  },
  formBody: {
    marginTop: 5,
    marginHorizontal: '5%',
  },
  roundBox: {
    borderRadius: 4,
    backgroundColor: colors.whiteB,
    padding: 2,
    marginBottom: 5,
  },
  textInput: {
    paddingLeft: 8
  },
  picker: {
    color: colors.whiteF,
    backgroundColor: colors.whiteB,
  },
  pickerItem: {
    fontSize: 15,
  },
  arrowDown: {
    position: 'absolute',
    zIndex: 1,
    right: 5,
    top: '18%',
    width: 24,
    height: 24,
    tintColor: colors.whiteF
  },
  inputTitle: {
    marginBottom: 4,
  },
  searchBtnContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 60,
    backgroundColor: colors.whiteA,
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 10,
    elevation: 4,
  },
  searchIcon: {
    width: 40,
    height: 40,
  },
  modalContainer: {
    backgroundColor: colors.whiteC,
    padding: 10,
    borderRadius: 4,
  },
  modalTitle: {
    paddingLeft: 10
  }
});
