import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../theme";

import AppText from "../../components/app-text";
import DetailBox from "../../components/detail-box";

import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addToMySpec } from "../../actions/my-spec";


const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addToMySpec: addToMySpec,
  }, dispatch);
}

class ProductDetail extends Component {

  _addToMySpec(product) {
    ToastAndroid.show('Add to my spec', ToastAndroid.SHORT);
    this.props.addToMySpec(product);
    Actions.reset('Builder-stack-tab');
    Actions.jump('Manual');
  }

  _renderAddButton() {
    if (this.props.showAddBtn == true) {
      return (
        <View style={{ position: "absolute", bottom: 10, right: 10 }}>
          <TouchableOpacity activeOpacity={0.5} style={local.addButton} onPress={() => { this._addToMySpec(this.props.product) }}>
            <Image style={local.addIcon} source={require('../../assets/icons/plus.png')}></Image>
          </TouchableOpacity>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={global.pageContainer}>
        <ScrollView contentContainerStyle={global.pageScrollView}>
          <View style={[global.card, local.card]}>
            <View style={local.productImageContainer}>
              <Image style={local.productImage}
                resizeMethod="resize"
                source={{ uri: this.props.product.ImgURL }}></Image>
            </View>

            <View style={local.title}>
              <AppText value={this.props.product.Title} bold numLine={4} size="m" />
              <View style={global.colContent}>
                <View style={local.priceContainer}>
                  <AppText value={this.props.product.Price + ' THB'} bold size="l" color={colors.greenB} />
                </View>
              </View>
            </View>
          </View>
          <DetailBox product={this.props.product} />
        </ScrollView>
        {this._renderAddButton()}
      </View>
    );
  }
}

export default connect(null, mapDispatchToProps)(ProductDetail);
