import { StyleSheet } from "react-native";

export const local = StyleSheet.create({
  spinner: {
    flex: 1,
    alignSelf: 'center'
  },
});
