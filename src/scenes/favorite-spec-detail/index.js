import React, { Component } from 'react';
import {
  Linking,
  View,
  ScrollView,
  Alert,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';

import { local } from "./style";
import { colors, global } from "../../theme";

import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { removeFromFavorite } from "../../actions/favorite-specs";

// Component
import AppText from "../../components/app-text";
import Button from "../../components/custom-button";
import FavoriteComponent from "../../components/favorite-component";
import Spinner from "react-native-spinkit";
import ProductFilter from "../../components/product-filter";
import Modal from "react-native-modal";

const mapStateToProps = (state) => {
  return {
    favoriteSpecs: state.favoriteSpecs
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    removeFromFavorite: removeFromFavorite,
  }, dispatch);
}

class FavoriteSpecDetail extends Component {
  state = {
    showDeleteModal: false,
  }

  _navigateToDetail(data) {
    Actions.push('Profile-product-detail', {
      product: data
    })
  }

  _navigateToStore(compType, requestName, componentIdx) {

    Actions.push('Profile-product-list', {
      title: compType,
      requestName,
      specIdx: this.props.specIdx,
      componentIdx,
      addMode: 'changeComp'
    });
  }

  //Use for linking the URL
  _navigateToCart(site) {
    Linking.openURL(site).catch(err => console.error('An error occured', err));
  }

  _confirmDelete() {
    this._closeModal();
    Actions.pop();
    const idx = this.props.specIdx;
    this.props.removeFromFavorite(idx);
  }

  _showModal() {
    this.setState({
      showDeleteModal: true,
    });
  }

  _closeModal() {
    this.setState({ showDeleteModal: false });
  }

  _renderItem() {
    const specIdx = this.props.specIdx;
    const spec = this.props.favoriteSpecs[specIdx];
    if (spec == undefined) { return null; }
    const components = spec.components;

    return (
      <FlatList
        data={components}
        contentContainerStyle={[global.pageFlatList, { paddingBottom: 75 }]}
        ListHeaderComponent={
          <View style={local.specDescription}>
            <View style={local.nameContainer}>
              <AppText bold size="l" value={spec.name} />
            </View>
            <View style={local.priceContainer}>
              <AppText color={colors.greenB} bold value={spec.price + " THB"} />
            </View>
          </View>}
        renderItem={({ item }) =>
          <FavoriteComponent
            key={item.key}
            name={item.Title}
            price={item.Price}
            type={item.productType}
            image={item.ImgURL}
            onPress={() => this._navigateToDetail(item)}
            onStore={() => this._navigateToStore(item.ComponentType, item.requestName, components.indexOf(item))}
            onCart={() => this._navigateToCart(item.CartURL)}>
          </FavoriteComponent>}
        keyExtractor={item => item.Title}
      />
    );

  }

  _renderModal() {
    return (
      <View>
        <Modal
          animationIn="fadeIn"
          animationOut="fadeOut"
          isVisible={this.state.showDeleteModal}
          onBackButtonPress={() => this._closeModal()}
          onBackdropPress={() => this._closeModal()}
        >
          <View style={[local.modalContainer]}>
            <View style={local.modalTitle}>
              <AppText noBorder value="Do you want to delete this spec" />
            </View>
            <View style={local.confirmContainer}>
              <TouchableOpacity style={local.confirmBtn} onPress={() => this._confirmDelete()}>
                <AppText size="sm" value="OK" center bold color={colors.whiteB} />
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  render() {
    return (
      <View style={global.pageContainer}>
        {this._renderModal()}
        {this._renderItem()}
        <View style={{ position: "absolute", bottom: 10, right: 10 }}>
          <TouchableOpacity activeOpacity={0.5} style={local.deleteBtn} onPress={() => { this._showModal() }}>
            <Image style={local.deleteIcon} source={require('../../assets/icons/close.png')}></Image>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteSpecDetail);