import {
  GET_RECOMMEND_FOR_AUTO,
  RESPONSE_GET_AUTO
} from "../actions/types";

import { getAutoRecommendBySocket } from "../actions/socket";
import { serviceSuccess, servicePending } from "../actions/request-api";


// Preset priority of pc type
const gamingType = {
  GPU: [1, {}],
  CPU: [2, {}],
  Mainboard: [3, {}],
  RAM: [4, {}],
  SSD: [5, {}],
  Harddisk: [5, {}],
  PowerSupply: [6, {}],
  Monitor: [7, {}]
};

const designerType = {
  CPU: [1, {}],
  GPU: [2, {}],
  RAM: [3, {}],
  Mainboard: [4, {}],
  SSD: [5, {}],
  Harddisk: [5, {}],
  Monitor: [6, {}],
  PowerSupply: [7, {}],
}

const presetPriority = {
  gaming: gamingType,
  designer: designerType,
}

export const socketService = store => next => action => {
  next(action);
  switch (action.type) {
    case GET_RECOMMEND_FOR_AUTO: {
      const pcType = action.payload.pcType;
      const budget = action.payload.budget;
      const postData = {
        budget: budget,
        priorities: presetPriority[pcType]
      }
      store.dispatch(servicePending());
      store.dispatch(getAutoRecommendBySocket(postData));
    } break;

    case RESPONSE_GET_AUTO: {
      store.dispatch(serviceSuccess(null)); // To stop spinner animation
    } break;

    default:
      break;
  }
}