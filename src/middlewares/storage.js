import {
  ADD_TO_FAVORITE,
  LOAD_FAVORITE_FROM_STORAGE,
  REMOVE_FROM_FAVORITE,
  CHANGE_COMPONENT,
} from "../actions/types";

import {
  AsyncStorage,
} from "react-native";

import { setFavoriteSpecs } from "../actions/favorite-specs";


async function saveFavoritespecsToStorage(favoriteSpecs) {
  try {
    await AsyncStorage.setItem('favoriteSpecs', JSON.stringify(favoriteSpecs));
  } catch (error) {
    alert(error);
  }
}

async function loadFavoritespecsFromStorage(store) {
  try {
    let favoriteSpecs = await AsyncStorage.getItem('favoriteSpecs');
    favoriteSpecs = await JSON.parse(favoriteSpecs);
    if (favoriteSpecs == null) {
      return;
    }
    store.dispatch(setFavoriteSpecs(favoriteSpecs));
  } catch (error) {
    alert(error);
  }
}

export const storageService = store => next => action => {
  next(action);
  switch (action.type) {
    case ADD_TO_FAVORITE: {
      const favoriteSpecs = store.getState().favoriteSpecs;
      saveFavoritespecsToStorage(favoriteSpecs);
    } break;

    case LOAD_FAVORITE_FROM_STORAGE: {
      loadFavoritespecsFromStorage(store);
    } break;

    case REMOVE_FROM_FAVORITE: {
      const favoriteSpecs = store.getState().favoriteSpecs;
      saveFavoritespecsToStorage(favoriteSpecs);
    } break;

    case CHANGE_COMPONENT: {
      const favoriteSpecs = store.getState().favoriteSpecs;
      saveFavoritespecsToStorage(favoriteSpecs);
    } break;

    default:
      break;
  }
}